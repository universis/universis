import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamsPreviewGeneralInstructorsComponent } from './exams-preview-general-instructors.component';

describe('ExamsPreviewGeneralInstructorsComponent', () => {
  let component: ExamsPreviewGeneralInstructorsComponent;
  let fixture: ComponentFixture<ExamsPreviewGeneralInstructorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamsPreviewGeneralInstructorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamsPreviewGeneralInstructorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
