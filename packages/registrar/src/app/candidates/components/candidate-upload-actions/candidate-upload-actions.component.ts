import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
// tslint:disable-next-line: max-line-length
import { AdvancedSearchFormComponent, AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableDataResult, AdvancedTableSearchComponent } from '@universis/ngx-tables';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-candidate-upload-actions',
  templateUrl: './candidate-upload-actions.component.html',
  styleUrls: []
})
export class CandidateUploadActionsComponent implements OnInit, OnDestroy {

  public recordsTotal: number | string;
  public tableConfiguration: any;
  public searchConfiguration: any;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;
  subscription: Subscription;

  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnDestroy(): void {
   if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  ngOnInit() {

    this.subscription = this.activatedRoute.data.subscribe(({ tableConfiguration, searchConfiguration }) => {
      // set search config
      this.searchConfiguration = searchConfiguration;
      this.search.form = this.searchConfiguration;
      // and init
      this.search.ngOnInit();
      // set table config
      this.tableConfiguration = tableConfiguration;
      this.table.config = AdvancedTableConfiguration.cast(this.tableConfiguration);
      // reset search text
      this.advancedSearch.text = null;
      // reset table
      this.table.reset(false);
    });
    
  }


  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

}
