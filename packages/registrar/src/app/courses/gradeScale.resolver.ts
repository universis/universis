import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { AngularDataContext } from "@themost/angular";
import { ConfigurationService } from "@universis/common";
import { AdvancedFormItemResolver } from "@universis/forms";

@Injectable({ providedIn: 'root' })
export class GradeScaleWithLocalesResolver implements Resolve<any> {
  constructor(private _context: AngularDataContext,
              private _itemResolver: AdvancedFormItemResolver,
              private _configurationService: ConfigurationService) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<any> | any {
    return this._itemResolver.resolve(route, state).then((item) => {
      if (Array.isArray(item.locales)) {
        // get index of object with inLanguage: defaultLocale
        const removeIndex = item.locales.findIndex(x => {
          return x.inLanguage === this._configurationService.settings.i18n.defaultLocale;
        });
        // remove object
        if (removeIndex >= 0) {
          item.locales.splice(removeIndex, 1);
        }

        // map locales
        for (const locale of this._configurationService.settings.i18n.locales) {
          if(locale !== this._configurationService.settings.i18n.defaultLocale) {
            // try to find  if localization data exist
            const findLocale = item.locales.find((itemLocale) => {
              return itemLocale.inLanguage === locale;
            });
            if (findLocale == null) {
              // if item does not exist add it
              item.locales.push({
                inLanguage: locale
              });
            }
          }
        }

        if (Array.isArray(item.values)) {
            item.values.forEach((gradeScaleValue: {locales: any[]}) => {
                if (Array.isArray(gradeScaleValue.locales)) {
                    const index = gradeScaleValue.locales.findIndex((x: any) => {
                        return x.inLanguage === this._configurationService.settings.i18n.defaultLocale;
                    });
                    // remove object
                    if (index >= 0) {
                        gradeScaleValue.locales.splice(index, 1);
                    }
                    // map locales
                    for (const locale of this._configurationService.settings.i18n.locales) {
                        if(locale !== this._configurationService.settings.i18n.defaultLocale) {
                            // try to find  if localization data exist
                            const findLocale = gradeScaleValue.locales.find((x: any) => {
                                return x.inLanguage === locale;
                            });
                            if (findLocale == null) {
                                // if item does not exist add it
                                gradeScaleValue.locales.push({
                                    inLanguage: locale
                                });
                            }
                        }
                    }
                }
            });
        }

      }
      return Promise.resolve(item);
    });
  }
}