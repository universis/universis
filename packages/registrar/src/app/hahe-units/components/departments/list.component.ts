import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { AdvancedRowActionComponent, AdvancedSearchFormComponent,
  AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableDataResult } from '@universis/ngx-tables';
import * as SpecializationTableConfiguration from './config.departments.json';
import * as DepartmentSearchConfiguration from './search.json';
import { AppEventService, DIALOG_BUTTONS, ErrorService, LoadingService, ModalService } from '@universis/common';
import { Observable, Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import {ClientDataQueryable} from '@themost/client';
import {AdminService, PermissionMask} from '@universis/ngx-admin';
@Component({
  selector: 'app-hahe-units-departments-list',
  templateUrl: './list.component.html',
  encapsulation: ViewEncapsulation.None
})
export class DepartmentsListComponent implements OnInit, OnDestroy {

  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();
  private selectedItems: any[];
  public canExportGraduates: boolean;
  recordsTotal: number;
  addSubscription: Subscription;

  constructor(private appEvent: AppEventService,
              private modalService: ModalService,
              private errorService: ErrorService,
              private translateService: TranslateService,
              private context: AngularDataContext,
              private loadingService: LoadingService,
              private _adminService: AdminService) {
    this.addSubscription = this.appEvent.added.subscribe((event: any) => {
      if (event && event.model === 'HaheUnits') {
        this.table.fetch();
      }
    });
  }

  ngOnDestroy(): void {
    if (this.addSubscription) {
      this.addSubscription.unsubscribe();
    }
  }

  async ngOnInit() {
    this.table.config = AdvancedTableConfiguration.cast(SpecializationTableConfiguration);
    this.search.form =   DepartmentSearchConfiguration;
    this.search.ngOnInit();
    // check if current user has permission to export graduates
    const canExportGraduates = {
      model: 'Department',
      privilege: 'Department/ExportGraduates',
      mask: PermissionMask.Execute
    };
    // do not await this call
    this._adminService.hasPermission(canExportGraduates).then((validationResult: boolean) => {
      this.canExportGraduates = validationResult;
    }).catch((err) => {
      console.error(err);
      this.canExportGraduates = false;
    });
  }

  /**
   * Data load event handler of advanced table component
   * @param {AdvancedTableDataResult} data
   */
  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  setHaheUnitAction() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      const component = this.modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      if (data.haheUnit == null) {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      }
      this.refreshAction.emit({
        progress: 1
      });
      const selectedItems = this.selectedItems.map((item) => {
        return {
          id: item.id
        };
      });
      (async () => {
        let haheUnit = data.haheUnit;
        if (haheUnit === '') {
          haheUnit = null;
        }
        for (let index = 0; index < selectedItems.length; index++) {
          try {
            const item = selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            await this.context.model('Departments').save({
              id: item.id,
              haheUnit: haheUnit
            });
            try {
              await this.table.fetchOne({
                id: item.id
              });
            } catch (err) {
              //
            }
          } catch (err) {
            // log error
            console.error(err);
            result.errors += 1;
          }
        }
        // clear selected
        this.table.selectNone();
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async setHaheUnit() {
    this.selectedItems = await this.getSelectedItems();
    this.modalService.openModalComponent(AdvancedRowActionComponent, {
      class: 'modal-lg',
      keyboard: false,
      ignoreBackdropClick: true,
      initialState: {
        items: this.selectedItems, // set items

        modalTitle: this.translateService.instant('HaheUnits.SetHaheUnitAction'), // set title
        description: this.translateService.instant('HaheUnits.SetHaheUnitDescription'),
        okButtonText: this.translateService.instant('Tables.Apply'),
        okButtonClass: 'btn btn-success',
        formTemplate: 'Departments/setHaheUnit',
        progressType: 'success',
        refresh: this.refreshAction, // refresh action event
        execute: this.setHaheUnitAction()
      }
    });
  }

  async exportGraduates() {
    const items = await this.getSelectedItems();
    // filter departments without haheUnit relation
    this.selectedItems = items.filter(item => item.haheUnit != null);
    this.modalService.openModalComponent(AdvancedRowActionComponent, {
      class: 'modal-lg',
      keyboard: false,
      ignoreBackdropClick: true,
      initialState: {
        items: this.selectedItems, // set items

        modalTitle: this.translateService.instant('HaheUnits.ExportGraduates'), // set title
        description: this.translateService.instant('HaheUnits.ExportGraduatesDescription'),
        okButtonText: this.translateService.instant('Tables.Apply'),
        okButtonClass: 'btn btn-success',
        formTemplate: 'Departments/exportGraduates',
        progressType: 'success',
        refresh: this.refreshAction, // refresh action event
        execute: this.exportGraduatesAction()
      }
    });
  }
  exportGraduatesAction() {
    return new Observable((observer) => {
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      const component = this.modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      if (data.academicYear == null) {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      }
      this.refreshAction.emit({
        progress: 1
      });
      const sendData =  {
        'academicYear': parseInt(data.academicYear, 0),
        'departments': []
      };
      for (let i = 0; i < this.selectedItems.length; i++) {
        const item = this.selectedItems[i];
        sendData.departments.push(item.id);
      }
      (async () => {
        this.loadingService.showLoading();
        this.context.model('HaheUnits/ExportGraduates').save(sendData).then((res) => {
          this.loadingService.hideLoading();
          const graduates = JSON.stringify(res);
          const blob = new Blob([graduates], { type: 'text/json' });
          const objectUrl = window.URL.createObjectURL(blob);
          const a = document.createElement('a');
          document.body.appendChild(a);
          a.setAttribute('style', 'display: none');
          a.href = objectUrl;
          a.download = `${data.academicYear}Graduates`;
          a.click();
          window.URL.revokeObjectURL(objectUrl);
          a.remove();
        }).catch(err => {
          this.loadingService.hideLoading();
          this.errorService.showError(err, {
            continueLink: '.'
          });
        });
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async getSelectedItems() {
    let items = [];
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          // get items
          const selectArguments = ['id', 'name'];
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .expand('haheUnit')
            .getItems();
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter( item => {
              return this.table.unselected.findIndex( (x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.table.selected.map( (item) => {
            return {
              id: item.id,
              name: item.name,
              haheUnit: item.haheUnit
            };
          });
        }
      }
    }
    return items;
  }

  clearHaheUnit() {
    if (this.table && this.table.selected && this.table.selected.length) {
      // get items to remove
      const items = this.table.selected.map((item) => {
        return {
          id: item.id,
          haheUnit: null
        };
      });
      return this.modalService.showWarningDialog(
        this.translateService.instant('HaheUnits.ClearHaheUnitTitle'),
        this.translateService.instant('HaheUnits.ClearHaheUnitMessage'),
        DIALOG_BUTTONS.OkCancel).then(result => {
        if (result === 'ok') {
          this.loadingService.showLoading();
          this.context.model('Departments').save(items).then(() => {
            this.table.fetch(true);
            this.loadingService.hideLoading();
            this.loadingService.hideLoading();
          }).catch(err => {
            this.loadingService.hideLoading();
            this.errorService.showError(err, {
              continueLink: '.'
            });
          });
        }
      });
    }
  }
}
