import { AfterViewInit, Component, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { BehaviorSubject, Observable, from } from 'rxjs';
import { flatMap, shareReplay } from 'rxjs/operators';
import { OneRosterReplaceClassesComponent } from './one-roster-replace-classes.component';
import { OneRosterReplacedByClassComponent } from './one-roster-replaced-by-class.component';

@Component({
  selector: 'app-one-roster-class-configuration',
  templateUrl: './one-roster-class-configuration.component.html',
})
export class OneRosterClassConfigurationComponent implements AfterViewInit, OnDestroy {

  public courseClass$: Observable<any[]> = 
  this.activatedRoute.params.pipe(flatMap(({ id }) => {
    return from(
      this.context.model('CourseClasses').select(
        'id', 'title', 'department/id as department', 'course/displayCode as displayCode', 'year/id as year', 'period/id as period'
      ).where('id').equal(id).expand('sections').getItem()
    );
  }), shareReplay());
  fragmentSubscription: any;

  constructor(
    private context: AngularDataContext,
    private activatedRoute: ActivatedRoute
  ) { }
  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    
  }
  ngAfterViewInit(): void {
    this.fragmentSubscription = this.activatedRoute.fragment.subscribe((fragment) => {
      const element = document.querySelector('#' + fragment);
      if (element) { element.scrollIntoView(); }
    });
  }

}