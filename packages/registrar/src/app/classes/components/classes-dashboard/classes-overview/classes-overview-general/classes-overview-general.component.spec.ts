import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassesOverviewGeneralComponent } from './classes-overview-general.component';

describe('ClassesOverviewGeneralComponent', () => {
  let component: ClassesOverviewGeneralComponent;
  let fixture: ComponentFixture<ClassesOverviewGeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassesOverviewGeneralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassesOverviewGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
