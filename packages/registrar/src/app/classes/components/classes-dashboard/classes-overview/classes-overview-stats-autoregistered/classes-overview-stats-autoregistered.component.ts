import {Component, OnDestroy, OnInit,ViewChild} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { TranslateService } from '@ngx-translate/core';
import {Subscription} from 'rxjs';
import { BaseChartDirective } from 'chart.js';

@Component({
  selector: 'app-classes-overview-stats-autoregistered',
  templateUrl: './classes-overview-stats-autoregistered.component.html',
  styleUrls: ['./classes-overview-stats-autoregistered.component.scss']
})
export class ClassesOverviewStatsAutoregisteredComponent implements OnInit, OnDestroy  {
  @ViewChild("baseChart") chart: BaseChartDirective;
  public courseClassStudents: any;
  private subscription: Subscription;

  public chartAutoregisteredStudentsOptions: any;
  public chartAutoregisteredStudentsLabels = [] ;
  public chartAutoregisteredStudentsData = [] ;
  public chartAutoregisteredStudentsType = 'doughnut';
  public chartAutoregisteredStudentsLegend = true;
  public chartAutoregisteredStudentsColours: Array<any> = [
    {
      backgroundColor: ['#63c2de', '#f86c6b', '#ffc107']
    }
  ];

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _translateService: TranslateService) { }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      if(this.chart !== undefined){
        this.chart.chart.data.datasets.forEach((dataset) => {
          dataset.data.pop();
        });
        this.chart.chart.data.labels.splice(0,this.chart.chart.data.labels.length);
        this.chartAutoregisteredStudentsLabels = [] ;
        this.chartAutoregisteredStudentsData = [];
      }
      this.courseClassStudents = await this._context.model('StudentCourseClasses')
        .where('courseClass').equal(params.id)
        .groupBy('autoRegistered')
        .select('count(id) as count, autoRegistered')
        .getItems();

      this.chartAutoregisteredStudentsOptions = {
        responsive: true,
        legend: {
          display: true,
          position: 'bottom'
        }
      };
      this.courseClassStudents.forEach(el => {
        this.chartAutoregisteredStudentsLabels.push(
          this._translateService.instant('AutoRegistrationStatus.' + el.autoRegistered.alternateName)
        );

        this.chartAutoregisteredStudentsData.push(el.count);
      });
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
