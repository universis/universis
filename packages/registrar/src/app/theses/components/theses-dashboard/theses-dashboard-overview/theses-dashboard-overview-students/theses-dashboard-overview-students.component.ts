import {Component, EventEmitter, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {TranslateService} from '@ngx-translate/core';
// tslint:disable-next-line:max-line-length
import {AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableDataResult} from '@universis/ngx-tables';
import {DIALOG_BUTTONS, ErrorService, ModalService, ToastService} from '@universis/common';
import {ActivatedTableService} from '@universis/ngx-tables';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-theses-dashboard-overview-students',
  templateUrl: './theses-dashboard-overview-students.component.html',
})
export class ThesesDashboardOverviewStudentsComponent implements OnInit, OnDestroy {

  private subscription: Subscription;
  public model: any;

  @ViewChild('students') students: AdvancedTableComponent;
  thesesID: any = this._activatedRoute.snapshot.params.id;

  constructor(private _activatedRoute: ActivatedRoute,
              private _activatedTable: ActivatedTableService,
              private _context: AngularDataContext) {}

  async ngOnInit() {
    this._activatedTable.activeTable = this.students;
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      const students = await this._context.model(`Theses/${params.id}/students`).asQueryable()
        .expand('student($expand=department,person,studentStatus)').getItems();
      this.model = Object.assign({
        id: parseInt(params.id, 10)
      }, {
        students
      });
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
