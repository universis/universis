import { Component, OnInit, OnDestroy, Input, ElementRef, ViewEncapsulation } from '@angular/core';

declare var $: any;
import { Subscription} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {TableConfigurationResolver} from '../../../../registrar-shared/table-configuration.resolvers';
import * as RESULTS_LIST_CONFIG from '../validate-results-table.config.list.json';

declare interface TablePathConfiguration {
  name: string;
  alternateName: string;
  show?: boolean;
}
@Component({
  selector: 'app-graduation-validations-home',
  templateUrl: './graduation-validations-home.component.html',
  encapsulation: ViewEncapsulation.None
})


export class GraduationValidationsHomeComponent implements OnInit, OnDestroy {
  public activePaths: Array<TablePathConfiguration> = [];
  public maxActiveTabs = 3;
  public tabs: Array<any> = [];
  private paramSubscription: Subscription;
  public paths: Array<TablePathConfiguration> = [];
  constructor(private _activatedRoute: ActivatedRoute,
              private _resolver: TableConfigurationResolver) { }

  ngOnInit() {
    this.paths = (<any>RESULTS_LIST_CONFIG).paths;
    this.activePaths = this.paths.filter( x => {
      return x.show === true;
    }).slice(0);
    this.paramSubscription = this._activatedRoute.firstChild.params.subscribe( params => {
      const matchPath = new RegExp('^list/' + params.list  + '$', 'ig');
      const findItem = this.paths.find( x => {
        return matchPath.test(x.alternateName);
      });
      if (findItem) {
        this.showTab(findItem);
      }
    });
  }

  showTab(item: any) {
    // find if path exists in active paths
    const findIndex = this.activePaths.findIndex( x => {
      return x.alternateName === item.alternateName;
    });
    if (findIndex < 0) {
      if (this.activePaths.length >= this.maxActiveTabs) {
        // remove item at index 0
        this.activePaths.splice(0, 1);
      }
      // add active path
      this.activePaths.push(Object.assign({}, item));
    }
  }

  ngOnDestroy(): void {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }

}


