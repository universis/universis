import { TableColumnConfiguration } from '@universis/ngx-tables';

export const REGISTRATION_SHARED_COLUMNS: TableColumnConfiguration[] = [
    {
        name: 'id',
        property: 'registrationId',
        title: 'Registrations.Shared.RegistrationId',
        hidden: true,
        optional: true
    },
    {
        name: 'registrationDate',
        property: 'registrationDate',
        title: 'Registrations.ListTable.RegistrationDate',
        formatter: 'DateTimeFormatter',
        formatString: 'shortDate',
        hidden: true,
        optional: true
    },
    {
        name: 'semester',
        property: 'registrationSemester',
        title: 'Registrations.Semester',
        hidden: true,
        optional: true
    },
    {
        name: 'notes',
        property: 'registrationNotes',
        title: 'Registrations.Shared.Notes',
        hidden: true,
        optional: true
    },
    {
        name: 'registrationYear/id',
        property: 'registrationYearId',
        title: 'Registrations.Shared.RegistrationYearId',
        hidden: true,
        optional: true
    },
    {
        name: 'registrationYear/alternateName',
        property: 'registrationYearName',
        title: 'Registrations.AcademicYear',
        hidden: true,
        optional: true
    },
    {
        name: 'registrationPeriod/id',
        property: 'registrationPeriodId',
        title: 'Registrations.Shared.RegistrationPeriodId',
        hidden: true,
        optional: true
    },
    {
        name: 'registrationPeriod/alternateName',
        property: 'registrationPeriodName',
        title: 'Registrations.AcademicPeriod',
        formatter: 'TranslationFormatter',
        formatString: 'Periods.${value}',
        hidden: true,
        optional: true
    },
    {
        name: 'status/alternateName',
        property: 'registrationStatusName',
        title: 'Registrations.Status',
        formatter: 'TranslationFormatter',
        formatString: 'RegistrationStatus.${value}',
        hidden: true,
        optional: true
    },
    {
        name: 'registrationType/id',
        property: 'registrationTypeId',
        title: 'Registrations.RegistrationTypeId',
        hidden: true,
        optional: true
    },
    {
        name: 'registrationType/name',
        property: 'registrationTypeName',
        title: 'Registrations.PeriodRegistrationType',
        hidden: true,
        optional: true
    }
];
