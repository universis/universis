{
  "title": "CourseClasses",
  "model": "CourseClasses",
  "searchExpression":"indexof(title, '${text}') ge 0 or indexof(course/displayCode, '${text}') ge 0 or indexof(course/department/abbreviation, '${text}') ge 0 or indexof(course/department/name, '${text}') ge 0 or indexof(year/alternateName, '${text}') ge 0 ",
  "selectable": true,
  "multipleSelect": true,
  "columns": [
    {
      "name": "id",
      "property": "id",
      "placeholder": "Classes.Preview",
      "formatter": "ButtonFormatter",
      "formatString": "#/classes/${id}",
      "className": "text-center",
      "formatOptions": {
        "buttonContent": "<i class=\"far fa-eye text-indigo\"></i>",
        "buttonClass": "btn btn-default",
        "commands": ["/classes", "${id}"]
      }
    },
    {
      "name": "id",
      "property": "rulesID",
      "placeholder": "Classes.Rules",
      "formatter": "ButtonFormatter",
      "formatString":"(modal:rules)",
      "className": "text-center",
      "formatOptions": {
        "buttonContent": "<i class=\"fas fa-blender text-indigo\"></i>",
        "buttonClass": "btn btn-default",
        "commands": [
          { "outlets":
          {
            "modal":["item", "${id}", "rules"]
          }
          }
        ],
        "navigationExtras": {
          "replaceUrl": false,
          "skipLocationChange": true
        }
      }
    },
    {
      "name":"course/displayCode",
      "property":"displayCode",
      "title":"Classes.DisplayCode"
    },
    {
      "name":"title",
      "property": "title",
      "title":"Classes.ClassTitle"
    },
    {
      "name":"year/alternateName",
      "property": "academicYear",
      "title":"Classes.AcademicYear"
    },
    {
      "name":"period/name",
      "property": "period",
      "title":"Classes.Period"
    },
    {
      "name":"numberOfStudents",
      "property": "numberOfStudents",
      "title":"Classes.NumberOfStudents"
    },
    {
      "name":"statistic/examined",
      "property": "examined",
      "title":"Classes.NumberOfStudentsExamined"
    },
    {
      "name":"statistic/passed",
      "property": "passed",
      "title":"Classes.NumberOfStudentsPassed"
    },
    {
      "name":"statistic/studyGuideUrl",
      "property": "studyGuideUrl",
      "title":"Classes.StudyGuideUrl",
      "hidden": true
    },
    {
      "name":"statistic/eLearningUrl",
      "property": "eLearningUrl",
      "title":"Classes.ELearningUrl",
      "hidden": true
    },
    {
      "name":"statistic/hasRules",
      "property": "hasRules",
      "title":"Classes.hasRules",
      "formatters": [
        {
          "formatter": "TranslationFormatter",
          "formatString": "${value != null ? (value?'Yes':'No'):'-'}"
        },

        {
          "formatter": "NgClassFormatter",
          "formatOptions": {
            "ngClass": {
              "text-secondary": "${hasRules==null}",
              "text-success": "${!!hasRules}",
              "text-danger": "${!hasRules}"
            }
          }
        }
      ]
    },
    {
      "name":"statistic/registrationType",
      "property": "registrationType",
      "title":"Classes.CourseClassStatistic.RegistrationType",
      "formatters": [
        {
          "formatter": "TranslationFormatter",
          "formatString": "RegistrationTypes.${value}"
        }
      ]
    },
    {
      "name":"weekHours",
      "property": "weekHours",
      "title":"Classes.WeekHours",
      "hidden": false
    },
    {
      "name":"totalHours",
      "property": "totalHours",
      "title":"Classes.TotalHours",
      "hidden": false
    },
    {
      "name":"status/alternateName",
      "property": "status",
      "title":"Classes.ClassStatus",
      "formatters": [
        {
          "formatter": "TranslationFormatter",
          "formatString": "ClassStatuses.${value}"
        }
      ]
    },
    {
      "name":"year/id",
      "property": "year",
      "title": "Classes.YearId",
      "hidden": true
    },
    {
      "name":"period",
      "property": "academicPeriod",
      "title": "Classes.PeriodId",
      "hidden": true
    },
    {
      "name": "Instructors",
      "property": "instructors",
      "virtual": true,
      "sortable": false,
      "title": "Classes.Instructors",
      "formatters": [
        {
          "formatter": "TemplateFormatter",
          "formatString": "${instructors.length ? instructors.map(x => x.instructor && (x.instructor.familyName + ' ' + x.instructor.givenName)).join(', ') : '-'}"
        }
      ]
    },
    {
      "name":"course",
      "title":"Classes.Course",
      "hidden": true
    },
    {
      "name": "maxNumberOfStudents",
      "property": "maxNumberOfStudents",
      "title": "Classes.MaxNumberOfStudents",
      "hidden": true,
      "optional": true
    },
    {
      "name": "minNumberOfStudents",
      "property": "minNumberOfStudents",
      "title": "Classes.MinNumberOfStudents",
      "hidden": true,
      "optional": true
    },
    {
      "name": "absenceLimit",
      "property": "absenceLimit",
      "title": "Classes.AbsenceLimit",
      "hidden": true,
      "optional": true
    },
    {
      "name": "mustRegisterSection",
      "property": "mustRegisterSection",
      "title": "Classes.MustRegisterSection",
      "formatter": "TrueFalseFormatter",
      "hidden": true,
      "optional": true
    }
  ],
  "defaults":{
    "filter": "(department eq ${currentDepartment} and period eq ${currentPeriod} and year eq ${currentYear})",
    "orderBy": "year desc,period desc,title asc",
    "expand": "instructors($expand=instructor)"
  },
  "criteria": [
    {
      "name": "title",
      "filter": "(indexof(title, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "maxNumberOfStudents",
      "filter": "${value >=1 ? '(maxNumberOfStudents gt 0)': '(maxNumberOfStudents eq null or maxNumberOfStudents le 0)'}",
      "type": "text",
      "title":  "Classes.MaxNumberOfStudents"
    },
    {
      "name": "displayCode",
      "filter": "(indexof(course/displayCode, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "yearId",
      "filter": "(year/id eq '${value}')",
      "type": "text"
    },
    {
      "name": "period",
      "filter": "(period/alternateName eq '${value}')",
      "type": "text"
    },
    {
      "name": "currentYear",
      "filter": "(year eq '${currentYear}')",
      "type": "text"
    },
    {
      "name": "status",
      "filter": "(status/alternateName eq '${value}')",
      "type": "text"
    },
    {
      "name": "hasRules",
      "filter": "(statistic/hasRules eq '${value}')",
      "type": "text"
    },
    {
      "name": "registrationType",
      "filter": "(statistic/registrationType eq '${value}')",
      "type": "text"
    }
  ],
  "searches" : [
    {
      "name": "AdvancedSearch.PreDefinedLists.openClasses",
      "filter": {
        "courseClassStatus": "open"
      }
    }
  ]
}
